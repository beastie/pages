           ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
               MY OPINIONS ON DIFFERENT LINUX DISTROS AND
                           OPERATING SYSTEMS

                                  anya
           ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


Table of Contents
─────────────────

Intro
List of distros and operating system
.. Devuan
.. Arch
.. Artix
.. Void
.. Fedora
.. Ubuntu/Ubuntu based distros
.. FreeBsd
.. Windows





Intro
═════

  Everything in this blog post is based on my opinion and experiences
  and it is not intended to hurt anyone <3


List of distros and operating system
════════════════════════════════════

Devuan
──────

  Devuan is a derivative of Debian without systemd.

  I didn’t like this distro very much (mostly because I don’t like
  Debian and Debian based distros), the package manager `apt' is pretty
  slow compared to other linux package managers such as pacman and
  xbps. Packages are also pretty outdated.


Arch
────

  Arch is a independent rolling release distro.

  Packages in the official repos are rarely ever outdated. It also has
  access to the AUR where you can basically find any software that
  supports linux. The AUR is maintained by the users which makes it a
  lot easier to port a package to it. However this does have some
  downsides such as unmaintained/broken packages, malware(this is not
  very common ,but it can happen) etc. Arch’s package manager called
  `pacman' is also very fast and supports paralell downloads. Overall
  Arch is a good distro with only one major downside which is of course
  *systemd*


Artix
─────

  Artix is a rolling release distro based on Arch.

  Artix has all the great benefits of Arch, but without systemd!!, you
  have the choice of *5* different init systems on Artix ,but I have
  only tried the `runit' init system. The only slight downside to Artix
  is that the packages are updated a day or two after Arch ,but it is
  not a very big issue. Unlike Arch, Artix has a simple and fast gui
  installer which some people may prefer. This was my main distro for
  quite a long time and I like it quite a bit. I recommend this distros
  for most non-beginners


Void
────

  Void is a independent /stable/ rolling release distro.

  Packages in the official repos are usually couple weeks behind
  upstream which is a pretty bad thing. No closed-source packages can be
  found in the default repos which is great however you can install
  closed-source software by adding their official nonfree repo. The
  packages available on the main repos are also pretty limited. However
  Void has something similar to the AUR called `xbps-src' which contains
  some additional packages ,but unlike the AUR you have to compile the
  packages yourself which may take a while.

  Void does have some great benefits such as using the very minimal and
  fast init system called `runit' instead of the bloated mess which is
  `systemd'. Void also has a Musl edition which uses a much more minimal
  and secure C library instead of `glibc'. The package manager on void
  called `xbps' is *really* fast however it does not support parallel
  downloads. Void also comes with a very simple and fast installer. Void
  is pretty decent distro ,but I stopped using it due to the outdated
  packages


Fedora
──────

  Fedora is a stable semi-annual release distro funded by Redhat, but
  maintained by the community(not really lol). I have no intention of
  using distro again mainly because its /+supported+/ controlled by a
  corporation and not individuals. It has a lot of undesirable things
  such as systemd, gnome, wayland and other future feature-creeps.

  However despite these negatives Fedora does have some
  positives. Fedora has a pretty *big* community so its pretty easy find
  help on any communication platform, Fedora also rolls out bugfixes,
  kernel updates and security fixes pretty fast which is awesome,
  however you have to keep in mind that Fedora is “stable” distro and
  thus you will not be getting any major feature updates in Fedora for
  most packages. Fedora also has a very straight-forward and simple
  installer which is great for beginners. Fedora also different flavours
  of distro with different desktop environments called “Spins” which
  makes it very easy for beginners to switch to linux and still have a
  familiar desktop experience.

  Fedora also has something similar to the AUR called Copr, but I did
  not use it during my time using Fedora therefore I cannot say much
  about it

  I do not like Fedora very much due to the negatives (mainly the
  corporation control) however, this is a *great* distro for beginners
  (aslong as you install one of the “Spins” instead of the default Gnome
  DE)


Ubuntu/Ubuntu based distros
───────────────────────────

  Ubuntu is +a coporate controlled hellhole+ a stable annual release
  distro made by Canonical. Like many linux users my first ever linux
  distro was Ubuntu and I do not like it at all. Ubuntu is also a
  corporate controlled distro like Fedora ,but unlike Fedora it has even
  bigger downsides which make it even worse. Ubuntu has all the
  downsides that Fedora has plus some added bonus downsides. Ubuntu uses
  the same package manager as Debian which is pretty slow.

  Ubuntu also *forces* the users to use their own /universal/ (not
  universal at all lmao) package format called `snap' by default, it is
  one of the worst ways to install software on to a linux system please
  for the sake of your computers boot time and resources please do not
  use this. `snap' is very buggy, slow, uses a lot of system resources,
  freezes and crashes a lot, which is why I initially though linux was
  buggy and unstable (both of which are not true) and for a added bonus
  this depends on systemd to function. It is quite easy for a
  experienced user to remove `snap' from the system and use a somewhat
  usable desktop, but for a new user (a lot of new users start with
  Ubuntu if you didnt know) it will be very frustrating due to `snap'.

  Ubuntu is also *entirely* managed by a corporation unlike Fedora which
  is atleast somewhat slightly managed by its community.

  Ubuntu also has outdated packages just like Debian which is its parent
  distro. I recommend that you *do not* use Ubuntu for anything


FreeBsd
───────

  I will write a seperate blog post about my experience with FreeBsd <3
  (my experience with FreeBsd has been very positive though :D )


Windows
───────

  Windows is so shitty and useless, that I don’t even need to explain
  why c:
